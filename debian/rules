#!/usr/bin/make -f
include /usr/share/dpkg/pkg-info.mk

export DH_VERBOSE=1

# Avoid embedded blobs.
export DH_GOLANG_EXCLUDES := asset

BUILDDIR := build
METAPKG := github.com/prometheus/common

BRANCH     := debian/sid
USER       := pkg-go-maintainers@lists.alioth.debian.org
BUILD_DATE := $(shell date --utc --date='@$(SOURCE_DATE_EPOCH)' \
    +%Y%m%d-%H:%M:%S)
GO_VERSION := $(shell go version | sed 's/go version \(\S*\).*/\1/')
BUILDFLAGS := -ldflags \
  " -X $(METAPKG)/version.Version=$(DEB_VERSION_UPSTREAM)\
    -X $(METAPKG)/version.Revision=$(DEB_VERSION)\
    -X $(METAPKG)/version.Branch=$(BRANCH)\
    -X $(METAPKG)/version.BuildUser=$(USER)\
    -X $(METAPKG)/version.BuildDate=$(BUILD_DATE)\
    -X $(METAPKG)/version.GoVersion=$(GO_VERSION)"

%:
	dh $@ --buildsystem=golang --with=golang --builddirectory=$(BUILDDIR)

BINNAME := $(DEB_SOURCE)
WHATIS := "$(BINNAME) \\- Prometheus gateway for push metrics"

override_dh_auto_build:
	dh_auto_build -- $(BUILDFLAGS)
	# Rename the binary to match the debian package.
	cp -v $(BUILDDIR)/bin/pushgateway $(BUILDDIR)/bin/$(BINNAME)
	build/bin/$(BINNAME) --help-man > build/$(BINNAME).1
	# Remove build user/build date/go version headers, which is ugly.
	sed -i -e '/^  /d' build/$(BINNAME).1
	# Fix whatis entry.
	sed -i '/^.SH "NAME"/,+1c.SH "NAME"\n'$(WHATIS) build/$(BINNAME).1

override_dh_auto_install:
	dh_install build/bin/$(BINNAME) usr/bin/
	dh_installman build/$(BINNAME).1
