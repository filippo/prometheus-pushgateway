prometheus-pushgateway (1.4.0+ds-1) unstable; urgency=medium

  * New upstream release.
  ** TLS and basic authentication to HTTP endpoints.
  * Update patches.
  * Update dependencies.

 -- Filippo Giunchedi <filippo@debian.org>  Thu, 28 Jan 2021 22:36:07 +0100

prometheus-pushgateway (1.3.0+ds-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Filippo Giunchedi ]
  * New upstream version
  ** Refresh patches
  ** Update CLI arguments
  * Depend on daemon | systemd-sys (Closes: #943770)

 -- Filippo Giunchedi <filippo@debian.org>  Sat, 21 Nov 2020 15:26:45 +0100

prometheus-pushgateway (1.0.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Update dependencies.
  * Update patches.
  * Automated cme fixes.
  * Set default persistence file path in code.
  * debian/default: Update CLI documentation.
  * Update initscript.
  * Increase compat level to 12; add needed Pre-Depends for sysvinit
  * Update my name.

 -- Martina Ferrari <tincho@debian.org>  Thu, 07 Nov 2019 10:57:30 +0000

prometheus-pushgateway (0.7.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.

 -- Martina Ferrari <tincho@debian.org>  Mon, 24 Dec 2018 03:48:20 +0000

prometheus-pushgateway (0.6.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Update patching for new embedded blobs library.
  * Refresh patches
  * Update client-golang dependency.
  * debian/control: Update Standards-Version with no changes.

 -- Martina Ferrari <tincho@debian.org>  Fri, 19 Oct 2018 15:46:23 +0000

prometheus-pushgateway (0.5.2+ds-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Update client_golang version.
  * debian/copyright: Update Excluded-Files.
  * Bump Standards-Version (no changes).

 -- Martina Ferrari <tincho@debian.org>  Tue, 07 Aug 2018 14:53:36 +0000

prometheus-pushgateway (0.5.0+ds-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Martina Ferrari ]
  * New upstream release.
  * Stop using pristine-tar.
  * Refresh patches.
  * Automated cme fixes.
  * Update dh compat level to 11.
  * Remove unused lintian override.
  * debian/control: Update dependencies.
  * debian/postinst: Stop changing permissions recursively.
  * debian/rules: Do not use dpkg-parsechangelog directly.
  * debian/rules: Generate man page.
  * debian/docs: Update documentation list.
  * debian/default: Update parameter format and description.
  * debian/copyright: Adjust Files-Excluded.

 -- Martina Ferrari <tincho@debian.org>  Thu, 24 May 2018 04:52:55 +0000

prometheus-pushgateway (0.4.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Update dependency name for golang-github-prometheus-client-golang-
    dev.
  * debian/control: Update Standards-Version (no changes).
  * debian/control: Mark package as autopkgtest-able.
  * Enable gbp to automatically create pristine-tars; refresh rules.

 -- Martina Ferrari <tincho@debian.org>  Sat, 15 Jul 2017 14:50:00 +0000

prometheus-pushgateway (0.3.1+ds-1) unstable; urgency=medium

  * Also remove state file on purge.
  * New upstream release.
  * Replace vendored jQuery with Debian version.

 -- Martina Ferrari <tincho@debian.org>  Mon, 26 Dec 2016 02:54:22 +0000

prometheus-pushgateway (0.3.0+ds-2) unstable; urgency=medium

  * Use a persistence file by default.
  * Add systemd service file.
  * Switch to using a single /usr/share/prometheus tree.
  * Add logrotate script.
  * Remove rotated logs on purge.

 -- Martina Ferrari <tincho@debian.org>  Mon, 27 Jun 2016 15:19:13 +0000

prometheus-pushgateway (0.3.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update Standards-Version with no changes.
    - Update Vcs-fields to use HTTPS.
    - Remove deprecated golang-github-prometheus-log-dev dependency, and add
      new golang-golang-x-sys-dev dependency.
    - Depend on golang-any, and current dh-golang.
  * Start using golang-github-prometheus-client-model-dev; remove
    related patch.
  * Refresh patches.
  * debian/rules: Simplify and update metadata.

 -- Martina Ferrari <tincho@debian.org>  Tue, 14 Jun 2016 14:22:03 +0000

prometheus-pushgateway (0.2.0+ds-3) unstable; urgency=medium

  * Update patches to use the client library from the common package.
    Closes: #811235.
  * Disable some flaky tests that might break builds.

 -- Martina Ferrari <tincho@debian.org>  Mon, 25 Jan 2016 23:49:58 +0000

prometheus-pushgateway (0.2.0+ds-2) unstable; urgency=medium

  * debian/rules: Make the build date match the changelog, to improve
    reproducibility.
  * debian/rules: Remove hostname from build so the package is finally
    reproducible, thanks to lamby@debian.org for the patch. Closes: #797622.

 -- Martina Ferrari <tincho@debian.org>  Tue, 01 Sep 2015 12:01:40 +0000

prometheus-pushgateway (0.2.0+ds-1) unstable; urgency=medium

  * Initial release. (Closes: #791720)

 -- Martina Ferrari <tincho@debian.org>  Tue, 07 Jul 2015 21:22:31 +0000
